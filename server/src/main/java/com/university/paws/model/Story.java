package com.university.paws.model;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Entity
@Table(name = "stories")
public class Story {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    private User user;

    @NotBlank
    @Size(min=3, max = 250)
    private String description;

    @ManyToOne(fetch=FetchType.LAZY)
    private AnnouncementStatus announcementStatus;

    public Story() {
    }

    public Story(User user, String description, AnnouncementStatus announcementStatus) {
        this.user = user;
        this.description = description;
        this.announcementStatus=announcementStatus;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public AnnouncementStatus getAnnouncementStatus() {
        return announcementStatus;
    }

    public void setAnnouncementStatus(AnnouncementStatus announcementStatus) {
        this.announcementStatus = announcementStatus;
    }
}
