package com.university.paws.model;

public enum AnnouncementTypeName {
    MATING,
    LOSE,
    SALE
}
