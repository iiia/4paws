package com.university.paws.model;

import org.hibernate.annotations.NaturalId;

import javax.persistence.*;

@Entity
@Table(name = "announcement_types", uniqueConstraints = {
        @UniqueConstraint(columnNames = {
                "name"
        })
})
public class AnnouncementType {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Enumerated(EnumType.STRING)
    @NaturalId
    @Column(length = 60)
    private AnnouncementTypeName name;
    public AnnouncementType() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public AnnouncementTypeName getName() {
        return name;
    }

    public void setName(AnnouncementTypeName name) {
        this.name = name;
    }
}