package com.university.paws.model;

import org.hibernate.annotations.NaturalId;

import javax.persistence.*;

@Entity
@Table(name = "announcement_statuses", uniqueConstraints = {
        @UniqueConstraint(columnNames = {
                "name"
        })
})
public class AnnouncementStatus {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Enumerated(EnumType.STRING)
    @NaturalId
    @Column(length = 60)
    private AnnouncementStatusName name;

    public AnnouncementStatus() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public AnnouncementStatusName getName() {
        return name;
    }

    public void setName(AnnouncementStatusName name) {
        this.name = name;
    }
}