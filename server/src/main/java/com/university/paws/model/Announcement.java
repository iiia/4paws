package com.university.paws.model;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Entity
@Table(name = "announcements")
public class Announcement {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(fetch=FetchType.LAZY)
    private AnnouncementType announcementType;

    @NotBlank
    @Size(min=3, max = 50)
    private String title;

    @NotBlank
    @Size(min=3, max = 250)
    private String description;

    @ManyToOne(fetch=FetchType.LAZY)
    private City city;

    private Double price;

    @ManyToOne(fetch=FetchType.LAZY)
    private Animal animal;

    @ManyToOne(fetch=FetchType.LAZY)
    private User user;

    @ManyToOne(fetch=FetchType.LAZY)
    private AnnouncementStatus announcementStatus;

    public Announcement(AnnouncementType announcementType, String title, String description, City city,
                        Double price, Animal animal, User user, AnnouncementStatus announcementStatus) {
        this.announcementType = announcementType;
        this.title = title;
        this.description = description;
        this.city = city;
        this.price = price;
        this.animal = animal;
        this.user = user;
        this.announcementStatus = announcementStatus;
    }

    public Announcement() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public AnnouncementType getAnnouncementType() {
        return announcementType;
    }

    public void setAnnouncementType(AnnouncementType announcementType) {
        this.announcementType = announcementType;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public City getCity() {
        return city;
    }

    public void setCity(City city) {
        this.city = city;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Animal getAnimal() {
        return animal;
    }

    public void setAnimal(Animal animal) {
        this.animal = animal;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public AnnouncementStatus getAnnouncementStatus() {
        return announcementStatus;
    }

    public void setAnnouncementStatus(AnnouncementStatus announcementStatus) {
        this.announcementStatus = announcementStatus;
    }
}

