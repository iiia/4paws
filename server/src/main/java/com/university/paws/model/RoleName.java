package com.university.paws.model;

public enum  RoleName {
    ROLE_USER,
    ROLE_ADMIN
}