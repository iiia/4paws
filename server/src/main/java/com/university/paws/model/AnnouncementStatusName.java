package com.university.paws.model;

public enum AnnouncementStatusName {
    CONFIRMED,
    CONSIDERED,
    REJECTED
}
