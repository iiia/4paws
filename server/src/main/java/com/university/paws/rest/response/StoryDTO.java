package com.university.paws.rest.response;

public class StoryDTO {
    private Long id;
    private String firstName;
    private String lastName;
    private String description;
    private String status;

    public StoryDTO() {

    }

    public StoryDTO(Long id,String firstName, String lastName, String description, String status){
        this.id=id;
        this.firstName=firstName;
        this.lastName=lastName;
        this.description=description;
        this.status=status;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
