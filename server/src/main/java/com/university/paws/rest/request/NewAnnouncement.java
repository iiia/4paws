package com.university.paws.rest.request;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

public class NewAnnouncement {
    @NotBlank
    @Size(min=3, max = 60)
    private String type;

    @NotBlank
    @Size(min=3, max = 50)
    private String title;

    @NotBlank
    @Size(min=3, max = 250)
    private String description;

    @NotBlank
    @Size(min=3, max = 50)
    private String city;

    @NotBlank
    private String price;

    @NotBlank
    private String age;

    @NotBlank
    private String breed;

    @NotBlank
    @Size(min=3, max = 50)
    private String username;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getBreed() {
        return breed;
    }

    public void setBreed(String breed) {
        this.breed = breed;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
