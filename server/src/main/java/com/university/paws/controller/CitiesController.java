package com.university.paws.controller;

import com.university.paws.model.City;
import com.university.paws.model.Story;
import com.university.paws.rest.response.StoryDTO;
import com.university.paws.services.CityService;
import com.university.paws.services.StoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.LinkedList;
import java.util.List;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/cities")
public class CitiesController {
    @Autowired
    CityService cityService;

    @GetMapping
    public ResponseEntity<?> listAllCities() {
        List<City> cities = cityService.getAllCities();
        List<String> citiesRes = new LinkedList<>();
        for(City city: cities){
            citiesRes.add(city.getName());
        }
        return ResponseEntity.ok(citiesRes);
    }
}
