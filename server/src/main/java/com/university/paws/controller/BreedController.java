package com.university.paws.controller;

import com.university.paws.model.Breed;
import com.university.paws.model.City;
import com.university.paws.services.BreedService;
import com.university.paws.services.CityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.LinkedList;
import java.util.List;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/breeds")
public class BreedController {
    @Autowired
    BreedService breedService;

    @PostMapping
    public ResponseEntity<?> listAllCities(@RequestBody String type) {
        List<Breed> breeds = breedService.getAllBreedsByType(type);
        List<String> breedsRes = new LinkedList<>();
        for(Breed breed: breeds){
            breedsRes.add(breed.getName());
        }
        return ResponseEntity.ok(breedsRes);
    }
}
