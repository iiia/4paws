package com.university.paws.controller;

import com.university.paws.model.AnimalType;
import com.university.paws.model.City;
import com.university.paws.services.AnimalTypeServise;
import com.university.paws.services.CityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.LinkedList;
import java.util.List;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/animalTypes")
public class AnimalTypesController {
    @Autowired
    AnimalTypeServise animalTypeServise;

    @GetMapping
    public ResponseEntity<?> listAllAnimalsTypes() {
        List<AnimalType> animalTypes = animalTypeServise.getAllAnimalTypes();
        List<String> animalTypesRes = new LinkedList<>();
        for(AnimalType animalType: animalTypes){
            animalTypesRes.add(animalType.getName());
        }
        return ResponseEntity.ok(animalTypesRes);
    }
}
