package com.university.paws.controller;

import com.university.paws.model.*;
import com.university.paws.rest.request.NewAnnouncement;
import com.university.paws.rest.request.NewStory;
import com.university.paws.rest.response.AnnouncementDTO;
import com.university.paws.rest.response.ResponseMessage;
import com.university.paws.rest.response.StoryDTO;
import com.university.paws.services.AnnouncementService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.LinkedList;
import java.util.List;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/announcement")
public class AnnouncementController {
    @Autowired
    AnnouncementService announcementService;

    @GetMapping("find/{id}")
    public ResponseEntity<?> getAnnouncement(@PathVariable long id) {
        Announcement announcement = announcementService.getAnnouncementById(id);
        AnnouncementDTO announcementDTO = new AnnouncementDTO(announcement.getId(),
                announcement.getUser().getFirstName(),
                announcement.getUser().getLastName(),
                announcement.getDescription(),
                announcement.getAnnouncementStatus().getName().toString(),
                announcement.getTitle(),
                announcement.getPrice().toString(),
                announcement.getAnimal().getBreed().getName(),
                announcement.getAnimal().getBreed().getAnimalType().getName(),
                announcement.getAnimal().getAge().toString(),
                announcement.getAnnouncementType().getName().toString(),
                announcement.getCity().getName(),
                announcement.getUser().getPhone());
        return ResponseEntity.ok(announcementDTO);
    }

    @GetMapping
    public List<AnnouncementDTO> listAllAnnouncement() {
        List<Announcement> announcements = announcementService.getAllAnnouncements();
        List<AnnouncementDTO> announcementDTOS = new LinkedList<>();
        for(Announcement announcement: announcements){
            AnnouncementDTO announcementDTO = new AnnouncementDTO(announcement.getId(),
                    announcement.getUser().getFirstName(),
                    announcement.getUser().getLastName(),
                    announcement.getDescription(),
                    announcement.getAnnouncementStatus().getName().toString(),
                    announcement.getTitle(),
                    announcement.getPrice().toString(),
                    announcement.getAnimal().getBreed().getName(),
                    announcement.getAnimal().getBreed().getAnimalType().getName(),
                    announcement.getAnimal().getAge().toString(),
                    announcement.getAnnouncementType().getName().toString(),
                    announcement.getCity().getName(),
                    announcement.getUser().getPhone());

            announcementDTOS.add(announcementDTO);
        }
        return  announcementDTOS;
    }

    @GetMapping("/considered")
    public ResponseEntity<?> listConsideredAnnouncements() {
        List<Announcement> announcements = announcementService.getAllAnnouncementsByStatus(AnnouncementStatusName.CONSIDERED);
        List<AnnouncementDTO> announcementDTOS = new LinkedList<>();
        for(Announcement announcement: announcements){
            AnnouncementDTO announcementDTO = new AnnouncementDTO(announcement.getId(),
                    announcement.getUser().getFirstName(),
                    announcement.getUser().getLastName(),
                    announcement.getDescription(),
                    announcement.getAnnouncementStatus().getName().toString(),
                    announcement.getTitle(),
                    announcement.getPrice().toString(),
                    announcement.getAnimal().getBreed().getName(),
                    announcement.getAnimal().getBreed().getAnimalType().getName(),
                    announcement.getAnimal().getAge().toString(),
                    announcement.getAnnouncementType().getName().toString(),
                    announcement.getCity().getName(),
                    announcement.getUser().getPhone());

            announcementDTOS.add(announcementDTO);
        }
        return  ResponseEntity.ok(announcementDTOS);
    }
    @PostMapping("/confirmed")
    public ResponseEntity<?>  listConfirmedAnnouncements(@RequestBody String type) {
        AnnouncementTypeName name = null;
        if (type.equals("mating")) {
            name = AnnouncementTypeName.MATING;
        } else if (type.equals("sale")){
            name = AnnouncementTypeName.SALE;
        } else if (type.equals("lost")){
            name = AnnouncementTypeName.LOSE;
        }
        System.out.println(name+ "**************************");
        List<Announcement> announcements = announcementService.getAllAnnouncementsByStatusAndType
                (AnnouncementStatusName.CONFIRMED, name);
        List<AnnouncementDTO> announcementDTOS = new LinkedList<>();
        for(Announcement announcement: announcements){
            AnnouncementDTO announcementDTO = new AnnouncementDTO(announcement.getId(),
                    announcement.getUser().getFirstName(),
                    announcement.getUser().getLastName(),
                    announcement.getDescription(),
                    announcement.getAnnouncementStatus().getName().toString(),
                    announcement.getTitle(),
                    announcement.getPrice().toString(),
                    announcement.getAnimal().getBreed().getName(),
                    announcement.getAnimal().getBreed().getAnimalType().getName(),
                    announcement.getAnimal().getAge().toString(),
                    announcement.getAnnouncementType().getName().toString(),
                    announcement.getCity().getName(),
                    announcement.getUser().getPhone());

            announcementDTOS.add(announcementDTO);
        }
        return  ResponseEntity.ok(announcementDTOS);
    }
    @PostMapping("/user")
    public ResponseEntity<?>  listAnnouncementsByUser(@RequestBody String username) {
        List<Announcement> announcements = announcementService.getAllAnnouncementsByUser(username);
        List<AnnouncementDTO> announcementDTOS = new LinkedList<>();
        for(Announcement announcement: announcements){
            AnnouncementDTO announcementDTO = new AnnouncementDTO(announcement.getId(),
                    announcement.getUser().getFirstName(),
                    announcement.getUser().getLastName(),
                    announcement.getDescription(),
                    announcement.getAnnouncementStatus().getName().toString(),
                    announcement.getTitle(),
                    announcement.getPrice().toString(),
                    announcement.getAnimal().getBreed().getName(),
                    announcement.getAnimal().getBreed().getAnimalType().getName(),
                    announcement.getAnimal().getAge().toString(),
                    announcement.getAnnouncementType().getName().toString(),
                    announcement.getCity().getName(),
                    announcement.getUser().getPhone());

            announcementDTOS.add(announcementDTO);
        }
        return  ResponseEntity.ok(announcementDTOS);
    }

    @PostMapping("/update")
    public ResponseEntity<?> updateAnnouncement(@RequestBody AnnouncementDTO announcementDTO) {
        announcementService.update(announcementDTO);
        return new ResponseEntity<>(new ResponseMessage("Announcement update successfully!"), HttpStatus.OK);
    }

    @PostMapping("/create")
    public ResponseEntity<?> createStory(@Valid @RequestBody NewAnnouncement newAnnouncement) {
        announcementService.create(newAnnouncement);
        return new ResponseEntity<>(new ResponseMessage("Story added successfully!"), HttpStatus.OK);
    }

    @DeleteMapping("delete/{id}")
    public ResponseEntity<?> deleteAnnouncement(@PathVariable long id) {
        announcementService.delete(id);
        return ResponseEntity.ok(HttpStatus.OK);
    }
}
