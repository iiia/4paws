package com.university.paws.controller;

import com.university.paws.rest.request.NewStory;
import com.university.paws.model.*;
import com.university.paws.rest.response.ResponseMessage;
import com.university.paws.rest.response.StoryDTO;
import com.university.paws.services.StoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.*;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/story")
public class StoryController {
    @Autowired
    StoryService storyService;

    @GetMapping
    public List<StoryDTO> listAllStories() {
        List<Story> stories =storyService.getAllStories();
        List<StoryDTO> storiesDTO=new LinkedList<>();
        for(Story story: stories){
            StoryDTO storyDTO1=new StoryDTO(story.getId(),story.getUser().getFirstName(), story.getUser().getLastName(),
                    story.getDescription(), story.getAnnouncementStatus().getName().toString());
            storiesDTO.add(storyDTO1);
        }
        return  storiesDTO;
    }

    @GetMapping("/considered")
    public ResponseEntity<?>  listConsideredStories() {
        List<Story> stories = storyService.getAllStoriesByStatus(AnnouncementStatusName.CONSIDERED);
        List<StoryDTO> storiesDTO=new LinkedList<>();
        for(Story story: stories){
            StoryDTO storyDTO1=new StoryDTO(story.getId(),story.getUser().getFirstName(), story.getUser().getLastName(),
                    story.getDescription(), story.getAnnouncementStatus().getName().toString());
            storiesDTO.add(storyDTO1);
        }
        return  ResponseEntity.ok(storiesDTO);
    }
    @GetMapping("/confirmed")
    public ResponseEntity<?>  listConfirmedStories() {
        List<Story> stories = storyService.getAllStoriesByStatus(AnnouncementStatusName.CONFIRMED);
        List<StoryDTO> storiesDTO=new LinkedList<>();
        for(Story story: stories){
            StoryDTO storyDTO1=new StoryDTO(story.getId(),story.getUser().getFirstName(), story.getUser().getLastName(),
                    story.getDescription(), story.getAnnouncementStatus().getName().toString());
            storiesDTO.add(storyDTO1);
        }
        return  ResponseEntity.ok(storiesDTO);
    }

    @PostMapping("/update")
    public ResponseEntity<?> updateStory(@RequestBody StoryDTO storyDTO) {
        storyService.update(storyDTO);
        return new ResponseEntity<>(new ResponseMessage("Story update successfully!"), HttpStatus.OK);
    }

    @PostMapping("/create")
    public ResponseEntity<?> createStory(@Valid @RequestBody NewStory newStory) {
        storyService.create(newStory);
        return new ResponseEntity<>(new ResponseMessage("Story added successfully!"), HttpStatus.OK);
    }

    @DeleteMapping("delete/{id}")
    public ResponseEntity<?> deleteUser(@PathVariable long id) {
        storyService.delete(id);
        return ResponseEntity.ok(HttpStatus.OK);
    }
}
