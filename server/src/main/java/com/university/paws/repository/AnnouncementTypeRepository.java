package com.university.paws.repository;

import com.university.paws.model.AnnouncementType;
import com.university.paws.model.AnnouncementTypeName;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface AnnouncementTypeRepository extends JpaRepository<AnnouncementType, Long> {
    Optional<AnnouncementType> findByName(AnnouncementTypeName announcementTypeName);
}
