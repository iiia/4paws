package com.university.paws.repository;

import com.university.paws.model.AnnouncementStatus;
import com.university.paws.model.AnnouncementStatusName;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface AnnouncementStatusRepository extends JpaRepository<AnnouncementStatus, Long> {
    Optional<AnnouncementStatus> findByName(AnnouncementStatusName announcementStatusName);
}
