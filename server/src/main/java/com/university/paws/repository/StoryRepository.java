package com.university.paws.repository;

import com.university.paws.model.AnnouncementStatus;
import com.university.paws.model.Story;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface StoryRepository extends JpaRepository<Story, Long> {
    Optional<Story> findByUserId(Long id);
    Optional<Story> findById(Long id);
    List<Story> findAll();
    List<Story> findByAnnouncementStatus(AnnouncementStatus status);
}
