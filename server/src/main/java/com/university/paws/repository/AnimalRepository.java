package com.university.paws.repository;

import com.university.paws.model.Animal;
import com.university.paws.model.Breed;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface AnimalRepository extends JpaRepository<Animal, Long> {
    Optional<Animal> findByBreed(Breed breed);
}
