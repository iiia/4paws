package com.university.paws.repository;

import com.university.paws.model.AnimalType;
import com.university.paws.model.Breed;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface BreedRepository extends JpaRepository<Breed, Long> {
    Optional<Breed> findByName(String name);
    List<Breed> findAllByAnimalType(AnimalType animalType);
}
