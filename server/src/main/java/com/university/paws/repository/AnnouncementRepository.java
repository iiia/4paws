package com.university.paws.repository;

import com.university.paws.model.Announcement;
import com.university.paws.model.AnnouncementStatus;
import com.university.paws.model.AnnouncementType;
import com.university.paws.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AnnouncementRepository extends JpaRepository<Announcement, Long> {
    List<Announcement> findAllByUser(User user);
    List<Announcement> findAllByAnnouncementStatus(AnnouncementStatus announcementStatus);
    List<Announcement> findAllByAnnouncementTypeAndAnnouncementStatus(AnnouncementType announcementType, AnnouncementStatus announcementStatus);
}
