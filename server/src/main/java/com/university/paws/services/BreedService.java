package com.university.paws.services;

import com.university.paws.model.AnimalType;
import com.university.paws.model.Breed;
import com.university.paws.model.Story;
import com.university.paws.repository.AnimalTypeRepository;
import com.university.paws.repository.BreedRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class BreedService {
    @Autowired
    BreedRepository breedRepository;

    @Autowired
    AnimalTypeRepository animalTypeRepository;

    @Transactional
    public List<Breed> getAllBreedsByType(String type){
        return breedRepository.findAllByAnimalType(animalTypeRepository.findByName(type)
                .orElseThrow(() -> new RuntimeException("Fail! -> Cause: Type don't  exist.")));
    }

}
