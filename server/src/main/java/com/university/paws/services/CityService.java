package com.university.paws.services;

import com.university.paws.model.City;
import com.university.paws.repository.CityRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class CityService {
    @Autowired
    CityRepository cityRepository;

    @Transactional
    public List<City> getAllCities(){
        return cityRepository.findAll();
    }
}
