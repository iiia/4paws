package com.university.paws.services;

import com.university.paws.model.AnnouncementStatus;
import com.university.paws.model.AnnouncementStatusName;
import com.university.paws.model.Story;
import com.university.paws.repository.AnnouncementStatusRepository;
import com.university.paws.repository.StoryRepository;
import com.university.paws.repository.UserRepository;
import com.university.paws.rest.request.NewStory;
import com.university.paws.rest.response.StoryDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class StoryService {
    @Autowired
    UserRepository userRepository;

    @Autowired
    StoryRepository storyRepository;

    @Autowired
    AnnouncementStatusRepository announcementStatusRepository;

    @Transactional
    public List<Story> getAllStories(){
        return storyRepository.findAll();
    }

    @Transactional
    public List<Story> getAllStoriesByStatus(AnnouncementStatusName name){
        AnnouncementStatus status = announcementStatusRepository.findByName(name)
                .orElseThrow(() -> new RuntimeException("Fail! -> Cause: Status don't  exist."));
        return storyRepository.findByAnnouncementStatus(status);
    }

    @Transactional
    public void update(StoryDTO storyDTO){
        AnnouncementStatus status;
        switch (storyDTO.getStatus()) {
            case "confirmed":
                status = announcementStatusRepository.findByName(AnnouncementStatusName.CONFIRMED)
                        .orElseThrow(() -> new RuntimeException("Fail! -> Cause: Status Confirmed not find."));
                break;
            case "considered":
                status = announcementStatusRepository.findByName(AnnouncementStatusName.CONSIDERED)
                        .orElseThrow(() -> new RuntimeException("Fail! -> Cause: Status Considered not find."));
                break;
            default:
                status = announcementStatusRepository.findByName(AnnouncementStatusName.REJECTED)
                        .orElseThrow(() -> new RuntimeException("Fail! -> Cause: Status Rejected not find."));
        }
        Story story = storyRepository.findById(storyDTO.getId())
                .orElseThrow(() -> new RuntimeException("Fail! -> Cause: Story don't exist."));
        story.setAnnouncementStatus(status);
        story.setDescription(storyDTO.getDescription());
        storyRepository.save(story);
    }

    @Transactional
    public void create(NewStory newStory){
        AnnouncementStatus status;
        switch (newStory.getStatus()) {
            case "confirmed":
                status = announcementStatusRepository.findByName(AnnouncementStatusName.CONFIRMED)
                        .orElseThrow(() -> new RuntimeException("Fail! -> Cause: Status Confirmed not find."));
                break;
            case "considered":
                status = announcementStatusRepository.findByName(AnnouncementStatusName.CONSIDERED)
                        .orElseThrow(() -> new RuntimeException("Fail! -> Cause: Status Considered not find."));
                break;
            default:
                status = announcementStatusRepository.findByName(AnnouncementStatusName.REJECTED)
                        .orElseThrow(() -> new RuntimeException("Fail! -> Cause: Status Rejected not find."));
        }
        Story story = new Story(userRepository.findByUsername(newStory.getUsername())
                .orElseThrow(()->new RuntimeException("Fail! -> Cause: User not find.")),
                newStory.getDescription(),status);

        storyRepository.save(story);
    }

    @Transactional
    public  void delete(Long id){
        storyRepository.deleteById(id);
    }

}
