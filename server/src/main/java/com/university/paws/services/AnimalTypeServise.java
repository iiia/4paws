package com.university.paws.services;

import com.university.paws.model.AnimalType;
import com.university.paws.model.City;
import com.university.paws.repository.AnimalTypeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class AnimalTypeServise {
    @Autowired
    AnimalTypeRepository animalTypeRepository;

    @Transactional
    public List<AnimalType> getAllAnimalTypes(){
        return animalTypeRepository.findAll();
    }
}
