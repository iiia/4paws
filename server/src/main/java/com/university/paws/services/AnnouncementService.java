package com.university.paws.services;

import com.university.paws.model.*;
import com.university.paws.repository.*;
import com.university.paws.rest.request.NewAnnouncement;
import com.university.paws.rest.response.AnnouncementDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class AnnouncementService {
    @Autowired
    UserRepository userRepository;

    @Autowired
    AnnouncementRepository announcementRepository;

    @Autowired
    AnnouncementStatusRepository announcementStatusRepository;

    @Autowired
    AnnouncementTypeRepository announcementTypeRepository;

    @Autowired
    CityRepository cityRepository;

    @Autowired
    BreedRepository breedRepository;

    @Autowired
    AnimalRepository animalRepository;

    @Transactional
    public Announcement getAnnouncementById(Long id){
        return announcementRepository.findById(id)
                .orElseThrow(() -> new RuntimeException("Fail! -> Cause: Announcement don't  exist."));
    }

    @Transactional
    public List<Announcement> getAllAnnouncements(){
        return announcementRepository.findAll();
    }

    @Transactional
    public List<Announcement> getAllAnnouncementsByUser(String username){
        return announcementRepository.findAllByUser(userRepository.findByUsername(username)
                .orElseThrow(() -> new RuntimeException("Fail! -> Cause: User don't  exist.")));
    }


    @Transactional
    public List<Announcement> getAllAnnouncementsByStatusAndType(AnnouncementStatusName name1, AnnouncementTypeName name2){
        AnnouncementStatus status = announcementStatusRepository.findByName(name1)
                .orElseThrow(() -> new RuntimeException("Fail! -> Cause: Status don't  exist."));
        AnnouncementType type = announcementTypeRepository.findByName(name2)
                .orElseThrow(() -> new RuntimeException("Fail! -> Cause: Type don't  exist."));
        return announcementRepository.findAllByAnnouncementTypeAndAnnouncementStatus( type, status);
    }

    @Transactional
    public List<Announcement> getAllAnnouncementsByStatus(AnnouncementStatusName name){
        AnnouncementStatus status = announcementStatusRepository.findByName(name)
                .orElseThrow(() -> new RuntimeException("Fail! -> Cause: Status don't  exist."));
        return announcementRepository.findAllByAnnouncementStatus(status);
    }

    @Transactional
    public void update(AnnouncementDTO announcementDTO){
        Announcement announcement = announcementRepository.findById(announcementDTO.getId())
                .orElseThrow(() -> new RuntimeException("Fail! -> Cause: Announcement don't exist."));
        AnnouncementType type;
        if(announcementDTO.getType().equals("Продаж")) {
            type = announcementTypeRepository.findByName(AnnouncementTypeName.SALE)
                    .orElseThrow(() -> new RuntimeException("Fail! -> Cause: Status Sale not find."));
        } else if(announcementDTO.getType().equals("Вязка")) {
            type = announcementTypeRepository.findByName(AnnouncementTypeName.MATING)
                    .orElseThrow(() -> new RuntimeException("Fail! -> Cause: Status Sale not find."));
        } else {
            type = announcementTypeRepository.findByName(AnnouncementTypeName.LOSE)
                    .orElseThrow(() -> new RuntimeException("Fail! -> Cause: Status Lose not find."));
        }
        System.out.println(type.getName()+"&&&&&&&&&&&&&&&&&&&&&&&&&&&");
        announcement.setAnnouncementStatus(getStatus(announcementDTO.getStatus()));
        announcement.setAnnouncementType(type);
        announcement.setTitle(announcementDTO.getTitle());
        announcement.setDescription(announcementDTO.getDescription());
        announcement.setCity(cityRepository.findByName(announcementDTO.getCity())
                .orElseThrow(() -> new RuntimeException("Fail! -> Cause: City don't exist.")));
        announcement.setPrice(Double.parseDouble(announcementDTO.getPrice()));

        announcementRepository.save(announcement);
    }

    @Transactional
    public void create(NewAnnouncement newAnnouncement){
        Animal animal = new Animal(Double.parseDouble(newAnnouncement.getAge()),
                breedRepository.findByName(newAnnouncement.getBreed())
                        .orElseThrow(() -> new RuntimeException("Fail! -> Cause: Breed don't exist.")));
        animalRepository.save(animal);

        AnnouncementType type;
        if(newAnnouncement.getType().equals("Продаж")) {
            type = announcementTypeRepository.findByName(AnnouncementTypeName.SALE)
                    .orElseThrow(() -> new RuntimeException("Fail! -> Cause: Status Sale not find."));
        } else if(newAnnouncement.getType().equals("Вязка")) {
            type = announcementTypeRepository.findByName(AnnouncementTypeName.MATING)
                    .orElseThrow(() -> new RuntimeException("Fail! -> Cause: Status Sale not find."));
        } else {
                type = announcementTypeRepository.findByName(AnnouncementTypeName.LOSE)
                        .orElseThrow(() -> new RuntimeException("Fail! -> Cause: Status Lose not find."));
        }
        Announcement announcement = new Announcement(type, newAnnouncement.getTitle(),
                newAnnouncement.getDescription(),cityRepository.findByName(newAnnouncement.getCity())
                .orElseThrow(() -> new RuntimeException("Fail! -> Cause: City don't exist.")),
                Double.parseDouble(newAnnouncement.getPrice()), animal, userRepository.findByUsername(newAnnouncement.getUsername())
                .orElseThrow(()->new RuntimeException("Fail! -> Cause: User not find.")),
                announcementStatusRepository.findByName(AnnouncementStatusName.CONSIDERED)
                .orElseThrow(()->new RuntimeException("Fail! -> Cause: Status not find.")));

        announcementRepository.save(announcement);
    }

    @Transactional
    public  void delete(Long id){
        announcementRepository.deleteById(id);
    }

    public AnnouncementStatus getStatus(String string){
        AnnouncementStatus status;
        switch (string) {
            case "confirmed":
                status = announcementStatusRepository.findByName(AnnouncementStatusName.CONFIRMED)
                        .orElseThrow(() -> new RuntimeException("Fail! -> Cause: Status Confirmed not find."));
                break;
            case "considered":
                status = announcementStatusRepository.findByName(AnnouncementStatusName.CONSIDERED)
                        .orElseThrow(() -> new RuntimeException("Fail! -> Cause: Status Considered not find."));
                break;
            default:
                status = announcementStatusRepository.findByName(AnnouncementStatusName.REJECTED)
                        .orElseThrow(() -> new RuntimeException("Fail! -> Cause: Status Rejected not find."));
        }
        return status;
    }
}
