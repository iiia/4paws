import { Component, OnInit } from '@angular/core';
import {TokenStorageService} from '../auth/token-storage.service';

@Component({
  selector: 'app-categories',
  templateUrl: './categories.component.html',
  styleUrls: ['./categories.component.css']
})
export class CategoriesComponent implements OnInit {
  private authority: string;
  private roles: string[];
  constructor(private tokenStorage: TokenStorageService) { }

  categ1: string;
  categ2: string;
  categ3: string;

  ngOnInit() {
    this.categ1 = 'sale';
    this.categ2 = 'mating';
    this.categ3 = 'lost';
    if (this.tokenStorage.getToken()) {
      this.roles = this.tokenStorage.getAuthorities();
      this.roles.every(role => {
        if (role === 'ROLE_ADMIN') {
          this.authority = 'admin';
          return false;
        }
        this.authority = 'user';
        return true;
      });
    }
  }

}
