import { Component, OnInit } from '@angular/core';
import {GalleryService} from '../services/gallery.service';
import {Annountcement} from '../gallery/gallery.model';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.css']
})
export class DetailComponent implements OnInit {
  announcement: Annountcement;
  id: string;
  errorMessage: string;
  constructor(private activateRoute: ActivatedRoute,private galleryService: GalleryService) {
    this.id = activateRoute.snapshot.params['id'];
  }

  ngOnInit() {
    this.galleryService.getAnnouncement(this.id).subscribe(
      data => {
        console.log(data);
        this.announcement = data;
        console.log(this.announcement);
      },
      error => {
        console.log(error);
        this.errorMessage = error.error.message;
      }
    );
  }

}
