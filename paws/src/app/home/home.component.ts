import { Component, OnInit } from '@angular/core';

import { TokenStorageService } from '../auth/token-storage.service';
import {NewStory, Story} from './home.model';
import {StoryService} from '../services/story.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  private roles: string[];
  private authority: string;
  form: any = {};
  errorMessage = '';
  private storyInfo: NewStory;
  isAdded = false;
  isAddFailed = false;
  stories: Story[];

  constructor(private tokenStorage: TokenStorageService, private storyService: StoryService) { }

  ngOnInit() {
    this.storyService.getStories().subscribe(
      data => {
        this.stories = data;
        console.log(this.stories);
      },
      error => {
        console.log(error);
        this.errorMessage = error.error.message;
      }
    );
    if (this.tokenStorage.getToken()) {
      this.roles = this.tokenStorage.getAuthorities();
      this.roles.every(role => {
        if (role === 'ROLE_ADMIN') {
          this.authority = 'admin';
          return false;
        }
        this.authority = 'user';
        return true;
      });
    }
  }
  onSubmit() {
    console.log(this.form);

    this.storyInfo = new NewStory(
      this.tokenStorage.getUsername(),
      this.form.description);

    console.log(this.storyInfo);
    this.storyService.createStory(this.storyInfo).subscribe(
      data => {
        console.log(data);
        this.isAdded = true;
        this.isAddFailed = false;
      },
      error => {
        console.log(error);
        this.errorMessage = error.error.message;
        this.isAddFailed = true;
      }
    );
  }
}
