export class Story {
  id: number;
  firstName: string;
  lastName: string;
  description: string;
  status: string;
}

export class NewStory {
  username: string;
  description: string;
  status: string;

  constructor(username: string, description: string) {
    this.username = username;
    this.description = description;
    this.status = 'considered';
  }
}
