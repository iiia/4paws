import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './home/home.component';
import {LoginComponent} from './login/login.component';
import {RegisterComponent} from './register/register.component';
import {CategoriesComponent} from './categories/categories.component';
import {AdminComponent} from './admin/admin.component';
import {UserComponent} from './user/user.component';
import {GalleryComponent} from './gallery/gallery.component';
import {NewAnnounComponent} from './new-announ/new-announ.component';
import {PageNotFoundComponent} from './page-not-found/page-not-found.component';
import {DetailComponent} from './detail/detail.component';

const routes: Routes = [
  {
    path: 'home',
    component: HomeComponent
  },
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: 'categories',
    component: CategoriesComponent
  },
  {
    path: 'categories/:category',
    component: GalleryComponent
  },
  {
    path: 'categories/:category/:id',
    component: DetailComponent
  },
  {
    path: 'newAnnouncement',
    component: NewAnnounComponent
  },
  {
    path: 'signup',
    component: RegisterComponent
  },
  {
    path: 'admin',
    component: AdminComponent
  },
  {
    path: 'user',
    component: UserComponent
  },
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  },
  { path: '**', component: PageNotFoundComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {
    scrollPositionRestoration: 'enabled',
    anchorScrolling: 'enabled',
  })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
