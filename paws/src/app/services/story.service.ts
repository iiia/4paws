import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import { Observable } from 'rxjs';
import {NewStory} from '../home/home.model';
const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};
@Injectable({
  providedIn: 'root'
})
export class StoryService {

  private allStory = 'http://localhost:8090/story/confirmed';
  private createStoryUrl = 'http://localhost:8090/story/create';

  constructor(private http: HttpClient) { }

  createStory(info: NewStory): Observable<string> {
    return this.http.post<string>(this.createStoryUrl, info, httpOptions);
  }
  getStories(): Observable<any> {
    return this.http.get(this.allStory);
  }

}
