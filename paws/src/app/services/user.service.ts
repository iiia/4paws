import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import { Observable } from 'rxjs';
import {NewStory, Story} from '../home/home.model';
import {Annountcement} from '../gallery/gallery.model';
const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};
@Injectable({
  providedIn: 'root'
})
export class UserService {

  private announcenmentUrl = 'http://localhost:8090/announcement/user';
  private updateStoryUrl = 'http://localhost:8090/story/update';
  private deleteAnnouncementUrl = 'http://localhost:8090/announcement/delete';

  constructor(private http: HttpClient) { }

  getAnnouncements(info: string): Observable<any> {
    return this.http.post(this.announcenmentUrl, info, httpOptions);
  }
  updateStory(info: Story): Observable<string> {
    return this.http.post<string>(this.updateStoryUrl, info, httpOptions);
  }
  deleteAnnouncement(id: string): Observable<any> {
    return this.http.delete(this.deleteAnnouncementUrl + '/' + id);
  }

}
