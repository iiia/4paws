import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import { Observable } from 'rxjs';
import {NewStory, Story} from '../home/home.model';
import {Annountcement} from '../gallery/gallery.model';
const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};
@Injectable({
  providedIn: 'root'
})
export class AdminService {

  private storyUrl = 'http://localhost:8090/story/considered';
  private  announcenmentUrl = 'http://localhost:8090/announcement/considered';
  private updateStoryUrl = 'http://localhost:8090/story/update';
  private updateAnnouncementUrl = 'http://localhost:8090/announcement/update';

  constructor(private http: HttpClient) { }

  updateAnnouncement(info: Annountcement): Observable<string> {
    return this.http.post<string>(this.updateAnnouncementUrl, info, httpOptions);
  }
  updateStory(info: Story): Observable<string> {
    return this.http.post<string>(this.updateStoryUrl, info, httpOptions);
  }
  getConsideredStories(): Observable<any> {
    return this.http.get(this.storyUrl);
  }
  getConsideredAnnouncement(): Observable<any> {
    return this.http.get(this.announcenmentUrl);
  }

}
