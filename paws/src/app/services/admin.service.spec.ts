import { TestBed, inject } from '@angular/core/testing';

import {expect} from '@angular/platform-browser/testing/src/matchers';
import {AdminService} from './admin.service';

describe('AdminService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AdminService]
    });
  });

  it('should be created', inject([AdminService], (service: AdminService) => {
    expect(service).toBeTruthy();
  }));
});
