import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import { Observable } from 'rxjs';
import {NewAnnountcement} from '../new-announ/new-announ.model';
const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};
@Injectable({
  providedIn: 'root'
})
export class GalleryService {

  private allAnnouncements = 'http://localhost:8090/announcement';
  private announcements = 'http://localhost:8090/announcement/confirmed';
  private allCities = 'http://localhost:8090/cities';
  private allAnimalTypes = 'http://localhost:8090/animalTypes';
  private breeds = 'http://localhost:8090/breeds';
  private createAnnouncementUrl = 'http://localhost:8090/announcement/create';
  private announcement = 'http://localhost:8090/announcement/find';

  constructor(private http: HttpClient) { }

  createAnnouncenment(info: NewAnnountcement): Observable<string> {
    return this.http.post<string>(this.createAnnouncementUrl, info, httpOptions);
  }
  getAnnouncement(id: string): Observable<any> {
    return this.http.get(this.announcement + '/' + id);
  }
  getAllAnnouncements(): Observable<any> {
    return this.http.get(this.allAnnouncements);
  }
  getAnnouncements(info: string): Observable<any> {
    return this.http.post(this.announcements, info, httpOptions);
  }
  getCities(): Observable<any> {
    return this.http.get(this.allCities);
  }
  getAnimalTypes(): Observable<any> {
    return this.http.get(this.allAnimalTypes);
  }
  getBreeds(info: string): Observable<any> {
    return this.http.post(this.breeds, info, httpOptions);
  }
}
