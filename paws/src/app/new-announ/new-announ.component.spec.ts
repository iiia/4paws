import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewAnnounComponent } from './new-announ.component';

describe('NewAnnounComponent', () => {
  let component: NewAnnounComponent;
  let fixture: ComponentFixture<NewAnnounComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewAnnounComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewAnnounComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
