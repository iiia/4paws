import { Component, OnInit } from '@angular/core';
import {GalleryService} from '../services/gallery.service';
import {NewAnnountcement} from './new-announ.model';
import {TokenStorageService} from '../auth/token-storage.service';

@Component({
  selector: 'app-new-announ',
  templateUrl: './new-announ.component.html',
  styleUrls: ['./new-announ.component.css']
})
export class NewAnnounComponent implements OnInit {
  form: any = {};
  private info: NewAnnountcement;
  isAdded = false;
  isAddFailed = false;
  cities: string[];
  animalTypes: string[];
  types: string[];
  breeds: string[];
  errorMessage: string;
  constructor(private tokenStorage: TokenStorageService, private galleryService: GalleryService) { }

  ngOnInit() {
    this.types = ['Продаж', 'Вязка', 'Загублено'];
    this.galleryService.getCities().subscribe(
      data => {
        console.log(data);
        this.cities = data;
        console.log(this.cities);
      },
      error => {
        console.log(error);
        this.errorMessage = error.error.message;
      }
    );
    this.galleryService.getAnimalTypes().subscribe(
      data => {
        console.log(data);
        this.animalTypes = data;
        console.log(this.animalTypes);
      },
      error => {
        console.log(error);
        this.errorMessage = error.error.message;
      }
    );
  }
  onSubmit() {
    console.log(this.form);

    this.info = new NewAnnountcement(
      this.form.title,
      this.form.description,
      this.tokenStorage.getUsername(),
      this.form.type,
      this.form.city,
      this.form.price,
      this.form.age,
      this.form.breed);

    this.galleryService.createAnnouncenment(this.info).subscribe(
      data => {
        console.log(data);
        this.isAdded = true;
        this.isAddFailed = false;
      },
      error => {
        console.log(error);
        this.errorMessage = error.error.message;
        this.isAddFailed = true;
      }
    );

  }
  onChange(type) {
    console.log(type);
    this.galleryService.getBreeds(type).subscribe(
      data => {
        console.log(data);
        this.breeds = data;
        console.log(this.breeds);
      },
      error => {
        console.log(error);
        this.errorMessage = error.error.message;
      }
    );
  }
}
