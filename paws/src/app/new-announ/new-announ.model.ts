export class NewAnnountcement {
  title: string;
  description: string;
  username: string;
  type: string;
  city: string;
  price: string;
  age: string;
  breed: string;

  constructor(title: string, description: string, username: string, type: string,
              city: string, price: string, age: string, breed: string) {
    this.title = title;
    this.description = description;
    this.username = username;
    this.type = type;
    this.city = city;
    this.price = price;
    this.age = age;
    this.breed = breed;
  }
}
