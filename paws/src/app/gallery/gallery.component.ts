import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Annountcement} from './gallery.model';
import {GalleryService} from '../services/gallery.service';

@Component({
  selector: 'app-gallery',
  templateUrl: './gallery.component.html',
  styleUrls: ['./gallery.component.css']
})
export class GalleryComponent implements OnInit {

  categName: string;
  title: string;
  cities: string[];
  announcements: Annountcement[];
  errorMessage: string;
  constructor(private activateRoute: ActivatedRoute, private galleryService: GalleryService) {
    this.categName = activateRoute.snapshot.params['category'];
  }

  ngOnInit() {
    this.galleryService.getAnnouncements(this.categName).subscribe(
      data => {
        console.log(data);
        this.announcements = data;
        console.log(this.announcements);
      },
      error => {
        console.log(error);
        this.errorMessage = error.error.message;
      }
    );
    if (this.categName === 'sale') {
      this.title = 'Знайти друга';
    } else if (this.categName === 'mating') {
      this.title = 'Знaйти пару';
    } else if (this.categName === 'lost') {
      this.title = 'Загублені тваринки';
    }
  }
}
