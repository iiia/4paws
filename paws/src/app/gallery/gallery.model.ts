export class Annountcement {
  title: string;
  description: string;
  firstName: string;
  lastName: string;
  phone: string;
  type: string;
  city: string;
  price: string;
  age: string;
  breed: string;
  status: string;
}
