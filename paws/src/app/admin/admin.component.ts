import {Component, OnInit} from '@angular/core';
import {AdminService} from '../services/admin.service';
import {Story} from '../home/home.model';
import {Annountcement} from '../gallery/gallery.model';
import {TokenStorageService} from '../auth/token-storage.service';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})
export class AdminComponent implements OnInit {
  stories: Story;
  announcements: Annountcement;
  errorMessage: string;
  private roles: string[];
  private authority: string;
  info: any;

  constructor(private adminService: AdminService, private tokenStorage: TokenStorageService) {
  }

  ngOnInit() {
    if (this.tokenStorage.getToken()) {
      this.info = {
        token: this.tokenStorage.getToken(),
        username: this.tokenStorage.getUsername(),
        authorities: this.tokenStorage.getAuthorities()
      };
      this.roles = this.tokenStorage.getAuthorities();
      this.roles.every(role => {
        if (role === 'ROLE_ADMIN') {
          this.authority = 'admin';
          return false;
        }
        this.authority = 'user';
        return true;
      });
    }
    this.adminService.getConsideredStories().subscribe(
      data => {
        console.log(data);
        this.stories = data;
        console.log(this.stories);
      },
      error => {
        console.log(error);
        this.errorMessage = error.error.message;
      }
    );
    this.adminService.getConsideredAnnouncement().subscribe(
      data => {
        console.log(data);
        this.announcements = data;
        console.log(this.announcements);
      },
      error => {
        console.log(error);
        this.errorMessage = error.error.message;
      }
    );
  }

  approve(story) {
    let s: Story;
    s = story;
    s.status = 'confirmed';
    console.log(s);
    this.adminService.updateStory(s).subscribe(
      data => {
        console.log(data);
        window.location.reload();
      },
      error => {
        console.log(error);
        this.errorMessage = error.error.message;
      }
    );
  }

  reject(story) {
    let s: Story;
    s = story;
    s.status = 'rejected';
    console.log(s);
    this.adminService.updateStory(s).subscribe(
      data => {
        console.log(data);
        window.location.reload();
      },
      error => {
        console.log(error);
        this.errorMessage = error.error.message;
      }
    );
  }

  approveA(story) {
    let s: Annountcement;
    s = story;
    s.status = 'confirmed';
    console.log(s);
    this.adminService.updateAnnouncement(s).subscribe(
      data => {
        console.log(data);
        window.location.reload();
      },
      error => {
        console.log(error);
        this.errorMessage = error.error.message;
      }
    );
  }

  rejectA(story) {
    let s: Annountcement;
    s = story;
    s.status = 'rejected';
    console.log(s);
    this.adminService.updateAnnouncement(s).subscribe(
      data => {
        console.log(data);
        window.location.reload();
      },
      error => {
        console.log(error);
        this.errorMessage = error.error.message;
      }
    );
  }


}
