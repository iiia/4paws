import { Component, OnInit } from '@angular/core';
import {Annountcement} from '../gallery/gallery.model';
import {Story} from '../home/home.model';
import {AdminService} from '../services/admin.service';
import {TokenStorageService} from '../auth/token-storage.service';
import {UserService} from '../services/user.service';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {
  announcements: Annountcement;
  errorMessage: string;
  private roles: string[];
  private authority: string;
  info: any;

  constructor(private userService: UserService, private tokenStorage: TokenStorageService) {
  }

  ngOnInit() {
    if (this.tokenStorage.getToken()) {
      this.info = {
        token: this.tokenStorage.getToken(),
        username: this.tokenStorage.getUsername(),
        authorities: this.tokenStorage.getAuthorities()
      };
      this.roles = this.tokenStorage.getAuthorities();
      this.roles.every(role => {
        if (role === 'ROLE_ADMIN') {
          this.authority = 'admin';
          return false;
        }
        this.authority = 'user';
        return true;
      });
    }
    this.userService.getAnnouncements(this.info.username).subscribe(
      data => {
        console.log(data);
        this.announcements = data;
        console.log(this.announcements);
      },
      error => {
        console.log(error);
        this.errorMessage = error.error.message;
      }
    );
  }

  delete(id){
    this.userService.deleteAnnouncement(id).subscribe(
      data => {
        console.log(data);
        window.location.reload();
      },
      error => {
        console.log(error);
        this.errorMessage = error.error.message;
      }
    );
  }
}
